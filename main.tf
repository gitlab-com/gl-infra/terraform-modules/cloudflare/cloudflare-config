resource "cloudflare_zone" "zone_definition" {
  account_id = var.cloudflare_account_id

  zone = var.cloudflare_zone_name
  plan = var.cloudflare_zone_plan
  type = var.cloudflare_zone_type
}

resource "cloudflare_zone_settings_override" "zone_override" {
  zone_id = cloudflare_zone.zone_definition.id

  settings {
    min_tls_version = "1.2"
    tls_1_3         = "on"
  }
}
